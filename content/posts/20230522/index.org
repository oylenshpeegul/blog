#+title: 70-year-old can opener
#+date: 2023-05-22
#+draft: false
#+tags[]:  

My Mom died recently. Amongst her things were my Dad's dogtags. They were on a chain with a key to some unknown padlock and this mysterious item.

#+ATTR_HTML: Dad's dogtags
[[./dads-dogtags.jpg]]

It's a can opener! More specifically, it's a [[https://en.wikipedia.org/wiki/P-38_can_opener][P-38]], which was issued with rations. It was originally intended to be disposable (it came with the rations and when you were done eating, you threw everything away), but it was so well designed, that folks kept them and re-used them. That included my Dad, apparently.

It folds flat like that, so you can keep it almost anywhere. Then it unfolds ninety degrees into a can opener.

#+ATTR_HTML: P-38 unfolded
[[./open.jpg]]

To use it, just unfold it, puncture the can, and start to work around bit by bit until it's open. 

#+ATTR_HTML: Start
[[./start.jpg]]

Easy peasy! I'm not sure why "better" can openers were developed since then. This seems perfect!

#+ATTR_HTML: Opwn
[[./done.jpg]]


I just made dinner with this 70-year-old can opener and it was wicked easy! Apparently, you can also use it as a flathead screwdriver. I think I will keep this in my pocket just in case!
