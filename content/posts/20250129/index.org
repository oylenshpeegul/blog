#+title: Lectures on the Philosophy of Mathematics
#+date: 2025-01-29
#+draft: false
#+tags[]: books math

I just finished watching the last of the [[https://jdh.hamkins.org/lectures-on-the-philosophy-of-mathematics-oxford-mt20/][Lectures on the Philosophy of Mathematics]] by Joel David Hamkins. Fantastic!

#+ATTR_HTML: :alt cover of Lectures on the Philosophy of Mathematics book
[[./book-cover.jpg]]

These were recorded during Oxford Michaelmas Term[fn:: Michaelmas Term? How quaint! I don't think I would even know what Michaelmas was if not for Jane Austen.] 2020 and follow [[https://mitpress.mit.edu/9780262542234/lectures-on-the-philosophy-of-mathematics/][a book of the same name]], which was published in March 2021. I guess that means the actual students in the class couldn't buy the book until after the class was over. I'm surprised he didn't just give everyone an electronic copy of whatever the manuscript was at that time.

There are eight lectures, which I guess was once a week for eight weeks. They were meant to be one hour lectures, but he went over time every week. Even so, he skipped over things that were in his notes. I think I will buy his book to see what else he has to say. It was /so good!/

I watched them one a day for eight days. I almost wish I made it last longer. I managed to resist watching more than one a day, but I guess I could have stretched it out a bit more. Perhaps not to eight weeks, but maybe to a couple weeks. Oh, well.

I think I had been exposed to most (all?) of the individual topics that were discussed in these lectures, but I'd never had a philosophy class that tied them all together like this. It was really terrific!

Someone like me is never going to see a real lecture at the University of Oxford so I'm thrilled when stuff like this ends up online.

Thanks to Bartosz Milewski for [[https://mathstodon.xyz/@BartoszMilewski/113872657812390891][posting about it]] on Mastodon!
