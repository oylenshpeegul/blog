#+title: Inline Assembly
#+date: 2022-02-24
#+draft: false
#+tags[]: cray asm rust programming

Today [[https://blog.rust-lang.org/2022/02/24/Rust-1.59.0.html][they released Rust 1.59]], which (among other things) stabilized [[https://blog.rust-lang.org/2022/02/24/Rust-1.59.0.html#inline-assembly][inline assembly]]!

This reminded me of [[https://oylenshpeegul.gitlab.io/blog/posts/20211217/][programming the Cray]] back in the day. The Cray C compiler allowed you to stick CAL (Cray Assembly Language) instructions inline. On the Cray, optimizing your code was all about getting it to vectorize. If it vectorized, life was good. If it failed to vectorize, you were miserable. When this happened, sometimes you could convince it to vectorize something it wasn't by dropping down into CAL.

The Cray C compiler got better and better over the years and at some point in the 1990s, my attempts at writing CAL invariably made my programs slower and I stopped doing it entirely.

I guess now I can look forward to slowing down my Rust programs by adding inline assembly!


