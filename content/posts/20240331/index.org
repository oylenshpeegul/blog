#+title: A House with Good Bones
#+date: 2024-03-31
#+draft: false
#+tags[]: books

I just finished [[https://en.wikipedia.org/wiki/A_House_with_Good_Bones][A House with Good Bones]] (2023) by T. Kingfisher. Delightful!

#+ATTR_HTML: cover of A House with Good Bones
[[./cover.jpg]]

This was my first exposure to T. Kingfisher. I don't remember how it ended up on my reading list, but I'm glad it did. She seems [[https://en.wikipedia.org/wiki/Ursula_Vernon#Other_books_for_older_audiences][very prolific]]. I'll have to try some more!

I listened to the audiobook read by [[https://en.wikipedia.org/wiki/Mary_Robinette_Kowal][Mary Robinette Kowal]]. I thought her different voices made the already likeable characters even more likeable.
