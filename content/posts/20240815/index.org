#+title: Jane Eyre 1970
#+date: 2024-08-15
#+draft: false
#+tags[]: books movies knitting

Last night I watched [[https://en.wikipedia.org/wiki/Jane_Eyre_(1970_film)][Jane Eyre]] (1970), directed by [[https://en.wikipedia.org/wiki/Delbert_Mann][Delbert Mann]]. It stars [[https://en.wikipedia.org/wiki/Susannah_York][Susannah York]] as Jane and [[https://en.wikipedia.org/wiki/George_C._Scott][George C. Scott]] as Rochester. I have seen several adaptations of /Jane Eyre/, but I didn't even know this one existed. It's terrific!

#+ATTR_HTML: Rochester and Jane
[[./rochester-jane.jpg]]

Apparently, it was released in theaters in the UK in 1970 and then on television in the US in 1971. [[https://en.wikipedia.org/wiki/John_Williams][John Williams]] won an Emmy Award for the music in 1972.

#+ATTR_HTML: Jane with Mary and Diana Rivers
[[./knitting.jpg]]

Kara Wilson plays Diana Rivers, who is seen knitting. Looks convincing; I think Wilson can really knit.
