#+title: Haunt Sweet Home
#+date: 2024-10-10
#+draft: false
#+tags[]: books

I just finished [[https://us.macmillan.com/books/9781250330260/hauntsweethome][Haunt Sweet Home]] by Sarah Pinsker. Wonderful!

#+ATTR_HTML: cover of Haunt Sweet Home
[[./cover.jpg]]

I first heard of Sarah as a musician here in Baltimore. But I've since learned she's a talented writer as well.

/Haunt Sweet Home/ is a terrific little story (it's only 176 pages) that's perfect for October!
