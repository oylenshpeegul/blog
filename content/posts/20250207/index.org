#+title: Killing Me Softly
#+date: 2025-02-07
#+draft: false
#+tags[]: movies knitting

Last night I watched [[https://en.wikipedia.org/wiki/Killing_Me_Softly_(film)][Killing Me Softly]] (2002). Interesting, but impossible to talk about without spoilers.

#+ATTR_HTML: :alt Killing Me Softly title card
[[./title-card.jpg]]

It was directed by [[https://en.wikipedia.org/wiki/Chen_Kaige][Chen Kaige]] and is the only film he's ever done in English. [[https://www.karalindstrom.com/][Kara Lindstrom]] wrote the screenplay, based on [[https://en.wikipedia.org/wiki/Killing_Me_Softly_(novel)][the novel by Nicci French]].

It's a riot of hand-knits! Not only does Heather Graham don a knitted wedding gown

#+ATTR_HTML: :alt Heather Graham in a knitted wedding gown
[[./gown.png]]

and get served tea from a knitted tea cozy

#+ATTR_HTML: :alt a tea pot with a knitted cozy
[[./tea-cozy.png]]

but there are three knitted sweaters! Joseph Fiennes wears a bulky orange sweater for a good chunk of the movie

#+ATTR_HTML: :alt Joseph Fiennes in knitted sweater
[[./fiennes-sweater.png]]

and both Natascha McElhone and Heather Graham have multi-colored sweaters under their coats in the graveyard scene.

#+ATTR_HTML: :alt McElhone and Graham with multi-colored sweaters
[[./mcelhone-graham-sweaters.png]]

I didn't see anyone actually knitting in the movie, but we can't have everything.
