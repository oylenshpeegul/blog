#+title: The Scarlet Pimpernel
#+date: 2023-12-03
#+draft: false
#+tags[]: movies knitting

I just watched "The Scarlet Pimpernel" (1934) starring Leslie Howard and Merle Oberon. So strange.

#+ATTR_HTML: :title Leslie Howard and Merle Oberon
[[./howard-oberon.jpg]]

It is about an English aristocrat who saves French aristocrats from the guillotine in 1792. Apparently, most of the people watching the beheadings were knitters (at least, that's what the English believed in 1934).[fn:1]

#+ATTR_HTML: :title Knitters watching aristocrats being beheaded.
[[./knitting.jpg]]

It is based on the 1905 novel of the same name by Baroness Orczy.

This is clearly where superheroes with secret identities come from. This is before Batman, before Superman, before the Phantom, before the Shadow, before Zorro. They were all different versions of the Scarlet Pimpernel!

Also, in the movie, Percy recites a poem that begins, "They seek him here / They seek him there." These are the first two lines of the 1966 Kinks song, "Dedicated Follower of Fashion." I've known that song almost all of my life, so I was shocked when those words showed up in this old movie!

#+ATTR_HTML: :title "The Scarlet Pimpernel" lobby card
[[./lobby-card.jpg]]

[fn:1] No, that was totally a thing!
[[https://lithub.com/on-the-covert-role-of-knitting-during-the-french-revolution-and-world-war-ii/][On the Covert Role of Knitting During the French Revolution and World War II]]
