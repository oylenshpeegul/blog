#+title: The Man Who Knew Too Much
#+date: 2023-11-29
#+draft: false
#+tags[]: movies

I just watched "The Man Who Knew Too Much" (1934), directed by Alfred Hitchcock. Peter Lorre is everything you've heard and more.

#+ATTR_HTML: Peter Lorre
[[./lorre.jpg]]

It gets its title from /The Man Who Knew Too Much: And Other Stories/ (1922) a book of detective stories by G. K. Chesterton. However, the movie has nothing else in common with the book. Hitchcock just used the title because he had the rights to it.

#+ATTR_HTML: Mummy's knitting
[[./knitting.jpg]]

There aren't any trains or knitting, /per se/, but there is a scene where Leslie Banks unravels "Mummy's knitting" by attaching the loose end to a dancer on the dance floor. What a monster!

#+ATTR_HTML: Nova Pilbeam
[[./pilbeam.jpg]]

The daughter is played by Nova Pilbeam, which has to be the most fun name I've heard in a long long time!

#+ATTR_HTML: "The Man Who Knew Too Much" poster
[[./poster.jpg]]

Weirdly, Hitchcock made another unrelated film of the same name in 1956 (no resemblance to the book or the 1934 movie).
