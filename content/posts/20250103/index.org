#+title: Lightweight Linux Distributions?
#+date: 2025-01-03
#+draft: false
#+tags[]: linux

I just read [[https://www.zdnet.com/article/5-lightweight-linux-distributions-with-very-low-system-requirements/][5 lightweight Linux distributions with very low system requirements]]. The five distributions mentioned are 

1. Linux Lite (Ubuntu with Xfce)
2. AntiX (Debian with IceWM)
3. Bodhi Linux (Ubuntu with Moksha)
4. BunsenLabs (Debian with OpenBox)
5. Lubuntu (Ubuntu with LXQt)

All five are derived from Debian. I find that plain old Debian with the Xfce window manager is terrific, even on very old and very weak computers. My main computer is a circa 2018 desktop that is modest, but not exactly low-powered. It could handle a fancier window manager, but I prefer Xfce.

#+ATTR_HTML: :alt screenshot of terminal showing neofetch output
[[./desktop.png]]

My wife recently got a new laptop and gave me her old one. It was an old Windows 8 computer that is no longer supported by Microsoft. I installed Debian 12 with Xfce and it works great. I mean, the screen is tiny and I don't really like the keyboard, but neither of those is the operating system's fault. It runs fine. Even the silly touchscreen works. I brought it home with me at Christmas and it was my daily driver for a week. Granted, I didn't have much to do that week besides [[https://adventofcode.com/][Advent of Code]], but I was perfectly happy with it.

#+ATTR_HTML: :alt screenshot of terminal showing neofetch output
[[./laptop.png]]

So, obviously, I think Debian is a fine choice. But I'm concerned about the lack of diversity in that list. Are there no other lightweight Linux distributions anymore? What happened to CentOS or Fedora or whatever the [[https://en.wikipedia.org/wiki/RPM_Package_Manager][RPM]] people are doing these days?

**Update (2025-01-07):** And [[https://www.zdnet.com/article/this-linux-distro-could-let-your-old-laptop-shine-on-after-windows-10s-sunset/][here's another lightweight Linux distribution]] based on Debian.

