#+title: Lisp syntax
#+date: 2024-01-18
#+draft: false
#+tags[]: fennel clojure emacs lisp

Over on [[https://exercism.org/][Exercism]], they have something called [[https://exercism.org/challenges/48in24][The #48in24 Challenge]]. I'm not exactly sure what it's about, but it started this week with the leap year problem.

This is a fun little problem. A leap year in the Gregorian calendar is a year that is divisible by 4, unless it's divisible by 100, in which case it's only a leap year if it's also divisible by 400.

  - 2023 was not a leap year (not divisible by 4).
  - 2024 is a leap year (divisible by 4, but not 100).
  - 1900 was not a leap year (divisible by 100, but not 400).
  - 2000 was a leap year (divisible by 400).

They don't have a Fennel track, but since [[http://localhost:1313/blog/posts/20240106/][I've been doing Fennel lately]], that's what I tried.

#+begin_src lisp
(fn leap-year? [year]
  (and (= (% year 4) 0)
       (or (~= (% year 100) 0)
           (= (% year 400) 0))))
#+end_src

Turns out I had already done it some years ago in [[https://clojure.org/][Clojure]], which is also a Lisp.

#+begin_src clojure
(defn leap-year? [year]
  (and (= (mod year 4) 0)
       (or (not= (mod year 100) 0)
           (= (mod year 400) 0))))
#+end_src

They also have an [[https://www.gnu.org/software/emacs/manual/html_node/elisp/index.html][Emacs Lisp]] track, so I did it there too.

#+begin_src elisp
(defun leap-year-p (year)
  (and (= (% year 4) 0)
       (or (not (= (% year 100) 0))
           (= (% year 400) 0))))
#+end_src

Now, there are other ways to do this, but these are all the same algorithm. Lisp is famous for its elegance and lack of syntax, yet I am struck by how many differences there are. Different keywords for /function/, different ways to write /not equals/, even different ways to spell /modulo/. And these examples are tiny. I guess there is more syntax in Lisp than I usually consider. And these are just three Lisps. There's also [[https://lisp-lang.org/][Common Lisp]] and [[https://www.scheme.org/][Scheme]] and [[https://racket-lang.org/][Racket]] and...

*Update*: As was pointed out to me [[https://mastodon.social/@xkummerer@chaos.social/111780146402427218][on Mastodon]], this isn't really a /syntax/ issue. The only new syntax above is the square brackets in Fennel and Clojure.

