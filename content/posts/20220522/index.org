#+title: feedread
#+date: 2022-05-22
#+draft: false
#+tags[]: debian linux

I wrote [[https://gitlab.com/oylenshpeegul/feedread][feedread]], a little program to download my podcasts. I run it with [[https://manpages.debian.org/bullseye/systemd/systemd.timer.5.en.html][a systemd user timer]]. This is kind of like a cron job.

I created a directory for it

#+begin_src 
mkdir -p ~/.config/systemd/user/
#+end_src

and created a ~feedread.service~ file there

#+begin_src conf
[Unit]
Description=Check RSS feeds for new podcasts

[Service]
ExecStart=/home/tim/rust/target/release/feedread --feeds=/home/tim/rust/feedread/feeds --download-history=/home/tim/rust/feedread/download_history
Type=oneshot
#+end_src

as well as a ~feedread.timer~ file

#+begin_src conf
[Unit]
Description=Check RSS feeds for new podcasts

[Timer]
OnCalendar=daily
Persistent=true
RandomizedDelaySec=1h

[Install]
WantedBy=timers.target
#+end_src

Then I started the service

#+begin_src 
loginctl enable-linger tim
systemctl --user daemon-reload
systemctl --user enable feedread.timer --now
#+end_src

I can check on the service with 

#+begin_src 
systemctl --user list-timers
#+end_src

or

#+begin_src 
systemctl --user status feedread
#+end_src

Now if there are any new podcasts, they'll be waiting for me when I wake up!
