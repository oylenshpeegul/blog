#+title: Emacs configuration
#+date: 2024-01-08
#+draft: false
#+tags[]: emacs fennel

The other day, [[https://oylenshpeegul.gitlab.io/blog/posts/20240106/][I talked about Fennel]] and incidentally how to configure Emacs for it. In particular, since Fennel is a Lisp, I enabled ~parinfer~ for it. Here is the ~fennel.el~ file I showed.

#+begin_src lisp
(add-hook 'fennel-mode-hook 'parinfer-rust-mode)

(add-hook 'fennel-mode-hook 'eglot-ensure)

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs '(fennel-mode . ("fennel-ls"))))
#+end_src

Later, I realized that I handle parinfer hooks in ~parinfer.el~, so I should just add Fennel there.

#+begin_src lisp
;; https://github.com/justinbarclay/parinfer-rust-mode
(use-package parinfer-rust-mode
  :ensure
  :hook clojure-mode
  :hook common-lisp-mode
  :hook emacs-lisp-mode
  :hook fennel-mode
  :hook lisp-mode
  :hook scheme-mode
  :init
  (setq parinfer-rust-auto-download t))
#+end_src

But that just leaves a couple of [[https://joaotavora.github.io/eglot/][eglot]] things in ~fennel.el~. In fact, with the advent of Emacs 29, most of my programming files are just one or two ~eglot~ things. Here are a couple of examples.

~zig.el~

#+begin_src lisp
(add-hook 'zig-mode-hook 'eglot-ensure)
#+end_src

~elixir.el~
#+begin_src lisp
(add-hook 'elixir-mode-hook 'eglot-ensure)

(add-to-list
 'eglot-server-programs
 '(elixir-mode "~/elixir/elixir-ls/language_server.sh"))
#+end_src

Maybe I should make an ~eglot.el~ file that does the hooks and things for all of the programming languages.

* My Emacs config
Some time ago, I adopted [[https://www.emacswiki.org/emacs/DotEmacsModular][this modular configuration]] strategy.

My init file just loads melpa and that ~load-directory~ function, which in turn loads everything in ~config~.

#+begin_src lisp
(when (>= emacs-major-version 27)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "https://melpa.org/packages/")
   t)

  ;; https://www.emacswiki.org/emacs/DotEmacsModular
  (load "~/.emacs.d/load-directory")
  (load-directory "~/.emacs.d/config"))
#+end_src

Then I can add and delete files from config all I want. Here's what it looks like now.

#+begin_src
❯ tree ~/.emacs.d/config/
/home/tim/.emacs.d/config/
├── avy.el
├── completions.el
├── dict.el
├── dired.el
├── ediff.el
├── emacs-gc-stats.el
├── encourage.el
├── frames.el
├── gnuplot.el
├── html.el
├── ligatures.el
├── magit.el
├── miri.el
├── multiple-cursors.el
├── nov.el
├── org.el
├── parinfer.el
├── personal.el
├── powerline
│   ├── LICENSE.md
│   ├── powerline.el
│   ├── powerline-separators.el
│   ├── powerline-themes.el
│   └── README.md
├── programming
│   ├── c.el
│   ├── elixir.el
│   ├── fennel.el
│   ├── go.el
│   ├── haskell.el
│   ├── julia.el
│   ├── lua.el
│   ├── perl.el
│   ├── python.el
│   ├── ruby.el
│   ├── rust.el
│   └── zig.el
├── pulse.el
├── rainbow-delimiters.el
├── recentf.el
├── rg.el
├── shrug.el
├── theme.el
├── tramp.el
├── vterm.el
├── whitespace.el
└── yasnippet.el

3 directories, 45 files
#+end_src

I'm pretty happy with it.

Now I'm thinking maybe I don't need the programming subdirectory. Perhaps just a ~programming.el~ file which does it all (mostly the eglot things, but some of the languages have non-eglot things too). Maybe I am overthinking this, but programming is by far the most important thing to me when it comes to Emacs configuration. 

I dunno. As we move to [[https://tree-sitter.github.io/tree-sitter/][tree-sitter]] based things, is all of this going to change again? Maybe I should just leave it as is. ¯\_(ツ)_/¯

