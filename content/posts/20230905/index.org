#+title: Ice Cream Cake
#+date: 2023-09-05
#+draft: false
#+tags[]:  

Every year, I make an ice cream cake for my wife's birthday. The first time I did it, I followed a "How to Make an Ice Cream Cake" video by Gretchen Siegchrist on About.com. But, like so many things on the web, that has disappeared. Here is what I do.

Ingredients:
- 1 package of oreos
- 1/4 cup butter
- 1/2 gallon of your favorite ice cream
- whipped cream
- a spring-form pan
- chocolate fudge (optional)

I have an ice cream maker, so instead of buying a 1/2 gallon of ice cream (which is usually only 1.5 quarts these days!), I make some.
  
[[https://www.allrecipes.com/recipe/51869/easy-mint-chocolate-chip-ice-cream/][Mint chocolate chip ice cream]]
- 2 cups 2% milk
- 2 cups heavy cream
- 1 cup sugar
- ½ teaspoon salt
- 1 teaspoon vanilla extract
- 1 teaspoon peppermint extract
- 1 cup miniature semisweet chocolate chips 

Here is what I use to make the whipped cream

Whipped cream
- 1 cup whipping cream
- 2 tblsp powdered sugar
- 1/2 tsp vanilla

Instructions: Set aside 10 or 12 cookies for decorating with later. Separate the rest from the cream filling and smash them into crumbs. Melt the butter and mix it into the cookie crumbs in a small bowl. Spread crumb mixture onto the bottom of the spring-form pan and place in the freezer for a few hours.

#+ATTR_HTML: cookie crumbs in spring-form pan
[[./cookie-crumbs-in-spring-form-pan.jpg]]

She softens her carton of ice cream, but I make the above recipe in the ice-cream maker. That takes about twenty minutes. Time it so that your spring-form pan is ready, because you pour the finished ice cream directly into the spring-form pan (on top of the frozen cookie-crumb crust). Put it back in the freezer for a few hours.

#+ATTR_HTML: ice-cream in spring-form pan
[[./ice-cream-in-spring-form-pan.jpg]]

Make the whipped cream. I use a mixer. I did it once with a whisk, but it was too hard. Remove the now frozen cake from the spring-form pan.

#+ATTR_HTML: whipped cream and cake
[[./whipped-cream-and-cake.jpg]]

Frost it with the whipped cream.

#+ATTR_HTML: whipped cream on cake
[[./whipped-cream-on-cake.jpg]]

Decorate as desired with cookies, crumbled cookies, chocolate fudge, &c. (I usually separate some cookies and stick the single cookies on the sides all the way around. I don't bother to remove the cream filling. Then I put a whole cookie in the center on top. Then crumble some more cookie all over the top. If you melt the fudge just right, you can drizzle squiqqly lines all over the top. In this picture, they came out as blobs.)

#+ATTR_HTML: finished cake
[[./finished-cake.jpg]]

Then put back in the freezer so that the whipped cream freezes.
