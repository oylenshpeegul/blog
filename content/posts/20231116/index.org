#+title: System Collapse
#+date: 2023-11-16
#+draft: false
#+tags[]: books murderbot martha wells

I just finished /System Collapse/ by Martha Wells. What joy! This is the latest installment of the Murderbot Diaries, probably my favorite story of the last few years (in any medium).

*Warning: spoilers ahead*

#+ATTR_HTML: cover of System Collapse
[[./cover.jpg]]

This is the first installment that takes place after /Network Effect/ (2020) (/Fugitive Telemetry/ came out after that (2021), but [[https://oylenshpeegul.gitlab.io/blog/posts/20220528/][it takes place before]]). This is huge if, like me, you are terribly concerned about Three. It was somewhere else for much of this story, but we did get an update on its status (all is well). There is much to look forward to, but for now I guess we can relax.

I am on the waiting list for the audiobook at the library. If that doesn't come up soon, I'll probably read the ink-on-paper version again! /I love Murderbot!/

#+ATTR_HTML: screenshot of audiobook
[[./audiobook.png]]

*Update: 2023-11-17* Now I'm listening to the audiobook. Like the earlier installments, it is read by Kevin R. Free!
