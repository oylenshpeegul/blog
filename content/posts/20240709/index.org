#+title: Seeing Double
#+date: 2024-07-09
#+draft: false
#+tags[]: movies

I just watched [[https://en.wikipedia.org/wiki/The_Monster_That_Challenged_the_World][The Monster that Challenged the World]] (1957), the second half of a double feature with [[https://en.wikipedia.org/wiki/The_Vampire_(1957_film)][The Vampire]], which I watched yesterday. Until recently, I didn't even know that double features were produced like this. That is, these two movies were made together and intended to be distributed as a double feature.

#+ATTR_HTML: Baseline Drive-in ad from 1957-06-26
[[./drive-in.jpg]]

The other day, I watched [[https://oylenshpeegul.gitlab.io/blog/posts/20240706/][Earth vs. the Flying Saucers]] and [[https://oylenshpeegul.gitlab.io/blog/posts/20240707/][The Werewolf]], which were a double feature in 1956 from a totally different company. Who knew?

I mean, we had double features at the drive-in when I was a kid, but I always just assumed they were put together by the drive-in, or maybe by the film distributor. I never dreamed they were filmed together!

It's also interesting that /The Monster that Challenged the World/ with /The Vampire/ is essentially the same double feature as /Earth vs. the Flying Saucers/ with /The Werewolf/, even though they were a year apart and from two different companies. In particular, /The Werewolf/ and /The Vampire/ were both classic movie monsters updated for the atomic age. That is, the monsters were created by science.

/The Vampire/ was particularly bad in that, with the exception of a few vampire bats in a cage, there was no mention of vampires. It was pretty much the Dr Jekyll and Mr Hyde story. It had the exact same ending as /The Werewolf/, with "Mr Hyde" transforming back into Dr Jekyll with identical stop-motion effects.

/The Monster that Challenged the World/ was marginally better than /The Vampire/ (just as /Earth vs. the Flying Saucers/ was marginally better than /The Werewolf/). It had an original script by [[https://en.wikipedia.org/wiki/David_Duncan_(writer)][David Duncan]], first called /The Jagged Edge/, then renamed /The Kraken/, before becoming /The Monster that Challenged the World/. Indeed, they mention the kraken several times during the movie, so perhaps they thought that was the name during filming.

Here Mimi Gibson and Audrey Dalton are being attacked by the monster.

#+ATTR_HTML: screen grab
#+CAPTION: Here's Johnny!
[[./heres-johnny.jpg]]
