#+title: The Trouble with Mrs Montgomery Hurst
#+date: 2024-07-27
#+draft: false
#+tags[]: books

I just finished [[https://www.katielumsden.co.uk/montgomery-hurst][The Trouble with Mrs Montgomery Hurst]] (2024) by Katie Lumsden. Wonderful. Just wonderful.

#+ATTR_HTML: cover of The Trouble with Mrs Montgomery Hurst
[[./cover.png]]

This is Lumsden's second novel. Like her first, [[https://oylenshpeegul.gitlab.io/blog/posts/20230306/][The Secrets of Hartwood Hall]], it is an historical novel that somehow feels very modern. Apart from that, it is not very similar.

This one takes place in 1841 and has a large cast of characters. Indeed, there is an entire made up county of Wickenshire and a whole lot going on; all of it fascinating.

I love listening to authors reading their own works. [[https://www.youtube.com/watch?v=IeSRPQ2t4RE][Here is Lumsden reading the first chapter of The Trouble with Mrs Montgomery Hurst]].
