#+title: MediaWiki API
#+date: 2023-01-24
#+draft: false
#+tags[]: rust

I've been playing with the [[https://en.wiktionary.org/wiki/Wiktionary:Main_Page][Wiktionary]] API. I stumbled upon this program to get [[https://github.com/sts10/homophones][homophones]] by scraping the web page. I thought, surely there must be an API for this. Turns out there isn't a Wiktionary-specific one, but as an instance of [[https://www.mediawiki.org/wiki/MediaWiki][MediaWiki]], it inherits that API. Wiktionary even supports the optional [[https://www.mediawiki.org/wiki/Extension:TextExtracts#API][extracts]] API, which I ended up using.

Here is a little command line program to get the homophones for a list of words.

#+begin_src rust
use anyhow::{bail, Result};
use tokio::task::JoinSet;

#[tokio::main]
async fn main() -> Result<()> {
    let arguments = std::env::args().collect::<Vec<_>>();
    if arguments.len() <= 1 {
        eprintln!("Usage: get-homophones ARG1 [ARG2 ...]");
        std::process::exit(1);
    }

    let mut set = JoinSet::new();
    for word in arguments[1..].iter() {
        set.spawn(get_homophones(word.to_string()));
    }

    while let Some(result) = set.join_next().await {
        let (word, homophones) = result??;
        println!("Homophones for {word}: {}", homophones.join(", "));
    }

    Ok(())
}

async fn get_homophones(word: String) -> Result<(String, Vec<String>)> {
    let api = mediawiki::api::Api::new("https://en.wiktionary.org/w/api.php").await?;

    let params = api.params_into(&[
        ("action", "query"),
        ("prop", "extracts"),
        ("exsentences", "10"),
        ("exlimit", "1"),
        ("explaintext", "1"),
        ("titles", &word),
    ]);

    let response = api.post_query_api_json(&params).await?;

    let Some(object) = response["query"]["pages"].as_object() else {
        bail!("Cannot find query pages object in response!");
    };

    let extract = object
        .iter()
        .map(|(_page_id, page)| page["extract"].as_str().unwrap())
        .collect::<String>();

    let homophones = extract
        .lines()
        .filter(|line| line.starts_with("Homophone"))
        .flat_map(|line| {
            let (_, h) = line.split_once(": ").unwrap();
            h.split(", ").map(|w| w.to_string())
        })
        .collect::<Vec<_>>();

    Ok((word, homophones))
}
#+end_src


It's kind of fun!

#+begin_src
❯ get-homophones adze ax acts witch
Homophones for witch: which (in accents with the wine-whine merger), wich, wych
Homophones for ax: 
Homophones for adze: adds, ads
Homophones for acts: 
#+end_src

I guess Wiktionary doesn't consider "ax" and "acts" homophones. ¯\_(ツ)_/¯
