#+title: The Shop Around the Corner
#+date: 2023-11-15
#+draft: false
#+tags[]: movies knitting

I just watched "The Shop Around the Corner" (1940) starring Margaret Sullavan (yes, with two a's) and James Stewart. Delightful!

#+ATTR_HTML: The Shop Around the Corner poster
[[./the-shop-around-the-corner.jpg]]

It also stars Frank Morgan, who played the Wizard (and Professor Marvel, and the gatekeeper, and the carriage driver, and the guard) in "The Wizard of Oz" (1939).

It was directed by Ernst Lubitsch, even though it only says produced by on the poster.

It is based on the 1937 play "Parfumerie" by Miklós László. Interestingly, "In the Good Old Summertime" (1949), "She Loves Me" (1963), and "You've Got Mail" (1998) were also based on Parfumerie. A very popular romcom!

*Update:* My sister points out that the book store in "You've Got Mail" was named The Shop Around the Corner, so that's pretty cool!

#+ATTR_HTML: Screen shot of Margaret Sullavan in a sweater
[[./sweater.jpg]]

Of course, as a knitter, I can't watch scenes like this without thinking, "Is that a hand-knit sweater? Those sleeves are interesting. What stitch is that? I bet I could make that."
