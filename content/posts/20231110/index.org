#+title: The Mummy
#+date: 2023-11-10
#+draft: false
#+tags[]: movies hammer horror

I just watched [[https://en.wikipedia.org/wiki/The_Mummy_(1959_film)][The Mummy]] (1959) with Christopher Lee as the mummy and Peter Cushing as the archaeologist.

#+ATTR_HTML: The Mummy poster
[[./the-mummy-poster.jpg]]

Like [[https://oylenshpeegul.gitlab.io/blog/posts/20231031/][The Curse of Frankenstein]] (1957) and [[https://oylenshpeegul.gitlab.io/blog/posts/20231102/][Horror of Dracula]] (1958), this was directed by Terence Fisher from a screenplay by Jimmy Sangster. It also features [[https://en.wikipedia.org/wiki/Yvonne_Furneaux][Yvonne Furneaux]] as both Princess Ananka and Isobel.

#+ATTR_HTML: The Mummy still with Yvonne Furneaux and Christopher Lee
[[./the-mummy-still.jpg]]

