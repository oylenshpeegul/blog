#+title: Clojure
#+date: 2021-09-19
#+draft: false
#+tags[]: debian bullseye linux emacs clojure programming

I haven't done a whole lot of [[https://clojure.org/][Clojure]] lately, but that's no reason not to install it. It officially supports Java LTS releases (currently, Java 11). I skipped 11 and installed OpenJDK 17 [[https://oylenshpeegul.gitlab.io/blog/posts/20210912/][the other day]]. That's not an LTS, but I think it will be okay. It also says, "Ensure that the following dependencies are installed: bash, curl, rlwrap, and Java." I think I have bash, curl, and Java, but I still need rlwrap

#+begin_src 
sudo apt install rlwrap
#+end_src

This is a wrapper for [[https://en.wikipedia.org/wiki/GNU_Readline][GNU Readline]], perhaps my favorite library ever.

Now we're ready to get Clojure!

#+begin_src 
curl -O https://download.clojure.org/install/linux-install-1.10.3.967.sh
chmod +x linux-install-1.10.3.967.sh
sudo ./linux-install-1.10.3.967.sh
rm linux-install-1.10.3.967.sh
#+end_src

That's that and that's that!

#+begin_src bash
❯ which clj
/usr/local/bin/clj

❯ which clojure
/usr/local/bin/clojure

❯ clojure -version
Clojure CLI version 1.10.3.967

❯ clj
Clojure 1.10.3
(println "Hello, world!")
Hello, world!
nil
#+end_src

Get clojure mode for Emacs.

#+begin_src 
M-x package-install RET clojure-mode RET
#+end_src

It has lots of customizations you can make

#+begin_src 
M-x customize-group RET clojure
#+end_src

by I didn't make any.

#+begin_src 
❯ cat hello.clj
(println "Hello, World!")

❯ clojure hello.clj 
WARNING: Implicit use of clojure.main with options is deprecated, use -M
Hello, World!

❯ clojure -M hello.clj 
Hello, World!
#+end_src

I guess I have to learn about ~-M~. Here's a shot of Emacs with clojure-mode in action

#+ATTR_HTML: :alt Emacs in Clojure mode
[[./emacs-clojure.png]]

Supergood!
