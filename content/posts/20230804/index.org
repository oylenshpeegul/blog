#+title: Tree pruning
#+date: 2023-08-04
#+draft: false
#+tags[]:

The other day we had a tree branch come down in a heavy wind.

#+ATTR_HTML: Broken limb
[[./broken-limb.jpg]]

It went all the way across the front yard and out into the road. Miraculously, nothing broke. It missed the front porch, the bird bath, the bird feeder...everything! We trimmed what we could from sticking out into the road, and called a [[https://boblesterjrtreeservice.com/][the tree guy]] to handle the rest. While the tree guy was here, we had him look at some of the trees in the back as well. Today they came out and did some pruning.

Here is the before and after for the maple tree on the right.

#+ATTR_HTML: Maple on the right
[[./right-maple.jpg]]

Here is the before and after for the maple tree on the left.

#+ATTR_HTML: Maple on the left
[[./left-maple.jpg]]

Here is the before and after for the back fence on the left.

#+ATTR_HTML: back fence on the left, before.
[[./left-fence-before.jpg]]

#+ATTR_HTML: back fence on the left, after.
[[./left-fence-after.jpg]]

Here is the before and after for the back fence on the right.

#+ATTR_HTML: back fence on the right, before.
[[./right-fence-before.jpg]]

#+ATTR_HTML: back fence on the right, after.
[[./right-fence-after.jpg]]







