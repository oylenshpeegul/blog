#+title: It's nice when people are nice.
#+date: 2023-10-07
#+draft: false
#+tags[]:  

Two bands I love a lot (I mean, really a lot) are [[http://www.thewoggles.com/][The Woggles]] and [[https://www.thehives.com/][The Hives]]. This week, Mighty Manfred (The Woggles) interviewed Pelle Almqvist (The Hives). The Hives have the Coolest Song of the Week this week on [[https://www.undergroundgarage.com/][the Underground Garage]] and Manfred does this every week.

At one point, Manfred said something about his band called "The Woggles" and Pelle said, "Oh, I love the Woggles! I didn't realize that was your band!" It was just lovely. Manfred was in his role as DJ, so of course Pelle can be forgiven for not knowing who he was speaking with. Both Manfred and Pelle seem like such lovely people with no ego problems that the whole thing just seemed so nice. The music business is filled with shitty people and shitty problems. You'd think it would be easier for people to just be kinder to each other, but apparently not. It's so nice when people are nice. 

[[https://www.youtube.com/watch?v=m2FtTIKK0TE][The Hives - Smoke & Mirrors]]

[[https://www.youtube.com/watch?v=3MVZW55XQ_k][The Hives - Smoke & Mirrors (live)]]
